//
//  TableViewCell.h
//  Table_View_Feedback
//
//  Created by Bruno Tavares on 14/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *secondaryLabel;
@property (nonatomic, strong) UITextField *mainField;
@property (nonatomic, strong) UIImageView *thumbView;

extern NSUInteger const kRowHeight;
extern NSUInteger const kImageCellOffset;
extern NSInteger const kAddCellOffset;
extern NSInteger const kSectionTitleOffset;
@end
