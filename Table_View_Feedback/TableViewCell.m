//
//  TableViewCell.m
//  Table_View_Feedback
//
//  Created by Bruno Tavares on 14/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // configure custom cell elements
        CGRect frame = CGRectMake(90, kImageCellOffset, 220, 25);
        self.mainField = [[UITextField alloc]initWithFrame:frame];
        self.mainField.font = [UIFont systemFontOfSize:18.0];
        self.mainField.textColor = [UIColor whiteColor];
      
        
        [self addSubview:self.mainField];
        
        CGRect imageFrame = CGRectMake(2*kImageCellOffset, kImageCellOffset, kRowHeight-(2*kImageCellOffset), kRowHeight-(2*kImageCellOffset));
        self.thumbView = [[UIImageView alloc] initWithFrame:imageFrame];
        
        self.thumbView.image = [UIImage imageNamed:@"198031.png"];
        [self addSubview:self.thumbView];
        
        frame.origin.y += 20;
        self.secondaryLabel = [[UILabel alloc]initWithFrame:frame];
        self.secondaryLabel.font = [UIFont systemFontOfSize:14.0];
        self.secondaryLabel.textColor = [UIColor grayColor];
        [self addSubview:self.secondaryLabel];
    }
    
    return self;
}



@end
