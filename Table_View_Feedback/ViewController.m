//
//  ViewController.m
//  Table_View_Feedback
//
//  Created by Bruno Tavares on 13/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"
#import "TableViewCell.h"

NSUInteger const kRowHeight = 60;
NSUInteger const kImageCellOffset = 10;
NSInteger const kAddCellOffset = -5;
NSInteger const kSectionTitleOffset = -20;

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray* tableData;
@property (nonatomic, strong) NSArray* sectionTitles;
@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) NSArray* deletedRows;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    
    //configure UITableView
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = kRowHeight;
    
    self.tableView.sectionHeaderHeight = 24.0;
    self.tableView.sectionFooterHeight = 24.0;
    
    self.tableData = [[NSMutableArray alloc] initWithObjects:
                      [[NSMutableArray alloc] initWithObjects:@"General", nil],
                      [[NSMutableArray alloc] initWithObjects:@"Displays", @"Trackpad", @"Mouse", @"Keyboard", nil],
                      [[NSMutableArray alloc] initWithObjects:@"iCloud", @"Network", @"Bluetooth", nil],
                      nil];
    
    self.sectionTitles = [[NSMutableArray alloc] initWithObjects:@"General Settings", @"Interface Settings", @"Connection Settings", nil];
    
    [self.view addSubview:self.tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.tableData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.tableData[section] count];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.tableData[indexPath.section] removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        if ([self.tableData[indexPath.section] count] == 0) {
            
            [self.tableData removeObjectAtIndex:indexPath.section];
            [self.tableView deleteSections:[[NSIndexSet alloc] initWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
            
    }
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //set cell label text
    cell.mainField.text = [[self.tableData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cell.secondaryLabel.text = [[self.tableData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    [cell.mainField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //cell styling
    cell.backgroundColor = [UIColor darkGrayColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Selected Row: %@", self.tableData[indexPath.section][indexPath.row]);
    
    [self.tableView reloadData];
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 400, tableView.rowHeight)];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(kImageCellOffset, kSectionTitleOffset, 400, tableView.rowHeight)];
    headerLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    headerLabel.text = self.sectionTitles[section];
    
    [headerView addSubview:headerLabel];
    
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-50, kAddCellOffset, tableView.rowHeight, tableView.rowHeight/2)];
    
    addButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addButton setTitle:@"+" forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(addCellToSection:) forControlEvents:UIControlEventTouchDown];
    
    [headerView addSubview:addButton];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 5.0f;
}

-(void) addCellToSection: (id) sender {
    
    UIButton *senderButton = (UIButton *)sender;

    CGPoint rootViewPoint = [senderButton.superview convertPoint:CGPointMake(senderButton.center.x, senderButton.center.y+50) toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:rootViewPoint];
    
    [self.tableData[indexPath.section] addObject:@"test"];
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    
    [self.tableView reloadData];
}

- (void) textFieldDidChange: (id) sender {
    
    UITextField * senderField = (UITextField *) sender;
    
    TableViewCell *cell = (TableViewCell *) senderField.superview;
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.tableData[indexPath.section][indexPath.row]  = cell.mainField.text;
}

@end
